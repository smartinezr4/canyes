package com.example.sergi.myapplication;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by sergi on 13-Mar-18.
 */

public class ClientActivity extends AppCompatActivity {

    private EditText serverIp;
    private Button connectPhones, sendBtn;
    private String serverIpAddress = "";
    private boolean connected = false;
    private Handler handler = new Handler();
    // DEFAULT IP
    public static String SERVERIP = "10.0.2.15";
    private TextView serverStatus;
    // DESIGNATE A PORT
    public static final int SERVERPORT = 8080;

    private ServerSocket serverSocket;

    //CHAT
    EditText message;
    TextView conversation;
    String messageTxt = "";
    Thread fst = new Thread(new ServerThread());


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client);
        serverStatus = findViewById(R.id.serverStatusClient);

        message = findViewById(R.id.message);
        conversation = findViewById(R.id.conversation);


        serverIp = findViewById(R.id.server_ip);
        connectPhones = findViewById(R.id.connect_phones);
        connectPhones.setOnClickListener(connectListener);

        sendBtn = findViewById(R.id.send);
        sendBtn.setOnClickListener(sendListener);

        ServerActivity server = new ServerActivity();
        SERVERIP = server.getLocalIpAddress();

        fst.start();

    }

    private View.OnClickListener connectListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            messageTxt = message.getText().toString();
            messageTxt += " \n";
            conversation.append(messageTxt);
            Thread cThread = new Thread(new ClientThread());
            //message.setText("");
            if (!connected) {
                serverIpAddress = serverIp.getText().toString();
                if (!serverIpAddress.equals("")) {
                    cThread.start();
                }
            }
        }
    };

    private View.OnClickListener sendListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            messageTxt = message.getText().toString();
            messageTxt += " \n";
            conversation.append(messageTxt);

        }
    };

    public class ClientThread implements Runnable {

        public void run() {
            try {
                InetAddress serverAddr = InetAddress.getByName(serverIpAddress);
                Log.d("ClientActivity", "C: Connecting...");
                Socket socket = new Socket(serverAddr, ServerActivity.SERVERPORT);
                connected = true;
                try {
                    Log.d("ClientActivity", "C: Sending command.");
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                    // WHERE YOU ISSUE THE COMMANDS
                    messageTxt = message.getText().toString();
                    out.println(messageTxt);
                    Log.d("ClientActivity", "C: Sent.");
                } catch (Exception e) {
                    Log.e("ClientActivity", "S: Error", e);

                }
                //socket.close();
                Log.d("ClientActivity", "C: Closed.");
            } catch (Exception e) {
                Log.e("ClientActivity", "C: Error", e);
                connected = false;
            }
        }
    }

    public class ServerThread implements Runnable {
        public void run() {
            try {
                if (SERVERIP != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            serverStatus.setText("Listening on IP: " + SERVERIP);
                        }
                    });
                    serverSocket = new ServerSocket(SERVERPORT);
                    while (true) {
                        // LISTEN FOR INCOMING CLIENTS
                        Socket client = serverSocket.accept();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                serverStatus.setText("Connected.");
                            }
                        });

                        try {
                            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                            String line = null;
                            while ((line = in.readLine()) != null) {
                            /*while (true) {
                                line = in.readLine();*/
                                Log.d("ServerActivity", line);
                                conversation.append("\n" + line);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        // DO WHATEVER YOU WANT TO THE FRONT END
                                        // THIS IS WHERE YOU CAN BE CREATIVE
                                    }
                                });
                            }
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    serverStatus.setText("Oops. Connection interrupted. Please reconnect your phones.");
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            serverStatus.setText("Couldn't detect internet connection.");
                        }
                    });
                }
            } catch (Exception e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        serverStatus.setText("Error");
                    }
                });
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            // MAKE SURE YOU CLOSE THE SOCKET UPON EXITING
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}