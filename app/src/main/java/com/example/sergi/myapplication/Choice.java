package com.example.sergi.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.Random;

/**
 * Created by sergi on 21-Feb-18.
 */

public class Choice extends AppCompatActivity {

    int num;
    Button btnYes, btnNo, btnClient;
    ImageView photo;
    Random r = new Random();
    int rMin = 1;
    int rMax = 16;
    String gender, preference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choice);

        gender = getIntent().getStringExtra("GENDER");
        preference = getIntent().getStringExtra("PREFERENCE");

        btnYes = findViewById(R.id.btnYes);
        btnNo = findViewById(R.id.btnNo);
        photo = findViewById(R.id.photo);
        getRndImage();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getRndImage();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getRndImage();
            }
        });

        Log.d("Gender", gender);
        Log.d("Preference", preference);

        btnClient = findViewById(R.id.btnClient);
        btnClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Choice.this, ClientActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getRndImage() {
        photo.setImageDrawable(null);
        num = r.nextInt(rMax - rMin + 1) + rMin;
        int genderRnd = r.nextInt(2 - 1 + 1) + 1;
        String genderRndStr = Integer.toString(genderRnd);

        switch (genderRndStr) {
            case "1":
                genderRndStr = "homes";
                break;
            case "2":
                genderRndStr = "dones";
                break;
        }

        switch (preference) {
            case "Homes":
                Glide.with(this).load("http://35.177.198.220/canyes/homes/" + num + ".jpg").into(photo);
                break;
            case "Dones":
                Glide.with(this).load("http://35.177.198.220/canyes/dones/" + num + ".jpg").into(photo);
                break;
            case "Els dos":
                Glide.with(this).load("http://35.177.198.220/canyes/" + genderRndStr + "/" + num + ".jpg").into(photo);
                break;
        }
    }
}
