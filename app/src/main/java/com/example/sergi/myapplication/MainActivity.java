package com.example.sergi.myapplication;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.net.ServerSocket;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button btnConfirm, btnServer, btnClient;
    RadioGroup rgGender, rgPreference;
    RadioButton radGenderM, radGenderF, radGenderO, radPrefM, radPrefF, radPrefO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();

/*
        btnServer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ServerActivity.class);
                startActivity(intent);
            }
        });

*/


        btnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                int selectedIdRG1 = rgGender.getCheckedRadioButtonId();
                RadioButton selectedRGen = findViewById(selectedIdRG1);

                int selectedIdRG2 = rgPreference.getCheckedRadioButtonId();
                RadioButton selectedRPref = findViewById(selectedIdRG2);

                String selectedRGenStr = selectedRGen.getText().toString();
                String selectedRPrefStr = selectedRPref.getText().toString();

                String sendGender = "", sendPreference = "";

                switch (selectedRGenStr) {
                    case "Home":
                        //Do something for men
                        sendGender = selectedRGenStr;
                        break;
                    case "Dona":
                        //Do something for women
                        sendGender = selectedRGenStr;
                        break;
                    case "Altre":
                        //Do something for women
                        sendGender = selectedRGenStr;
                        break;
                }

                switch (selectedRPrefStr) {
                    case "Homes":
                        //Likes Men
                        sendPreference = selectedRPrefStr;
                        break;
                    case "Dones":
                        //Likes Women
                        sendPreference = selectedRPrefStr;
                        break;
                    case "Els dos":
                        //Likes both
                        sendPreference = selectedRPrefStr;
                        break;
                }

                //Toast.makeText(MainActivity.this, selectedRPref.getText(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this, Choice.class);
                intent.putExtra("GENDER", sendGender);
                intent.putExtra("PREFERENCE", sendPreference);
                startActivity(intent);
            }
        });

    }

    private void initializeUI() {
        btnConfirm = findViewById(R.id.btnConfirm);
        rgGender = findViewById(R.id.rgGender);
        rgPreference = findViewById(R.id.rgPreference);
        //btnServer = findViewById(R.id.btnServer);
        btnClient = findViewById(R.id.btnClient);

        //Radio Buttons
        radGenderM = findViewById(R.id.gender_male);
        radGenderF = findViewById(R.id.gender_female);
        radGenderO = findViewById(R.id.gender_other);
        radPrefM = findViewById(R.id.choice_male);
        radPrefF = findViewById(R.id.choice_female);
        radPrefO = findViewById(R.id.choice_both);



    }


}
